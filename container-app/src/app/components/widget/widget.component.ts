import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit, Input } from '@angular/core';
import { IAppRegistration, IMicroAppCustomElement, IMicroAppsAPI } from '../../models/widget.types';
import { MicroAppsManagerService } from '../../services/micro-apps-manager.service';
import { MicroAppsStoreFacadeService } from '../../services/micro-apps-store-facade.service';
import { ScriptService } from '../../services/script.service';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() config: any; // global container config. Not used yet.

  selectedApp: IAppRegistration;
  registrations: IAppRegistration[];

  private loadedApps: Map<number | string, boolean> = new Map<number | string, boolean>();

  private destroy$ = new Subject<void>();

  constructor(
    private microAppsManagerService: MicroAppsManagerService,
    private microAppsStoreFacadeService: MicroAppsStoreFacadeService,
    private scriptService: ScriptService,
    @Inject(DOCUMENT) private document: Document
  ) {
  }

  ngOnInit(): void {
    this.microAppsStoreFacadeService.registraionListLoad();
    this.microAppsStoreFacadeService.getRegistraionList().pipe(
      takeUntil(this.destroy$)
    ).subscribe(registrations => {
      this.registrations = registrations;
      if (registrations.length > 0) {
        this.selectApp(registrations[0].id);
      }
    });
  }

  ngAfterViewInit(): void {
    this.microAppsStoreFacadeService.getSelectedApp().pipe(
      filter(app => !!app),
      takeUntil(this.destroy$)
    ).subscribe(app => {
      this.selectedApp = app;
      this.checkAndLoadApp(app);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectApp(registrationId: string): void {
    this.microAppsStoreFacadeService.selectApp(registrationId);
  }

  isAppSelected(registration: IAppRegistration): boolean {
    return !!this.selectedApp && registration.id === this.selectedApp.id;
  }

  getDOMElementId(appId: string): string {
    return `jj-micro-app-${appId}`;
  }

  private checkAndLoadApp(registration: IAppRegistration): void {
    if (this.isAppSourceLoaded(registration.id)) {
      return;
    }
    this.loadScript(registration.url).then(() => {
      this.markAppAsLoaded(registration.id);
      this.setUpCustomElement(registration);
    }).catch(e => console.log('err', e));
  }

  private loadScript(url: string): Promise<boolean> {
    return this.scriptService.loadScript(url);
  }

  private isAppSourceLoaded(appId: string): boolean {
    return this.loadedApps.has(appId);
  }

  private markAppAsLoaded(appId: string): void {
    this.loadedApps.set(appId, true);
  }

  private setUpCustomElement(registration: IAppRegistration): void {
    const el = this.document.getElementById(this.getDOMElementId(registration.id));
    el.innerHTML = '';
    const selectedAppEl = this.document.createElement(registration.tag) as IMicroAppCustomElement;
    this.setUpElementProps(selectedAppEl, registration);
    el.appendChild(selectedAppEl);
  }

  private setUpElementProps(element: IMicroAppCustomElement, registration: IAppRegistration): void {
    element.microAppsAPI = this.microAppsManagerService.createMicroAppsAPI(registration);
    element.appRegistration = registration;
  }

}
