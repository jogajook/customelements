import { HttpClientModule } from '@angular/common/http';
import { ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { createCustomElement } from '@angular/elements';
import { AppComponent } from './app.component';
import { WidgetComponent } from './components/widget/widget.component';
import { environment } from '../environments/environment';
import { MicroAppsEffects } from './store/micro-apps.effects';
import * as fromRoot from './store/micro-apps-reducer';

import { StoreModule } from '@ngrx/store';
import * as fromStore from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
window['store'] = fromStore;
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

/**
 * Important:
 * This app can work as an angular app or a simple custom element holder
 * You can comment out index.html <app-micro-apps-widget-ce></app-micro-apps-widget-ce> to enable custom element.
 * Comment out <app-comp></app-comp> and  app.bootstrap(AppComponent) to enable angular component
 * Custom element are stored in the folder custom-elements.
 * They have package.json config that bundles them and
 * copies main.js (it includes all needed) into the app folder under src/assers/apps
 * You should remove enableProdMode from custom element apps to prevent error when loading.
 * Projects (including this one) use ngx-build-plus
 * to handle external dependencies - https://www.angulararchitects.io/aktuelles/your-options-for-building-angular-elements/
 */

@NgModule({
  declarations: [
    AppComponent,
    WidgetComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({ [fromRoot.microAppsKey]: fromRoot.reducer }, {
      // runtimeChecks: {
      //   strictStateImmutability: true,
      //   strictActionImmutability: true
      // }
    }),
    EffectsModule.forRoot([MicroAppsEffects]),
    // StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {

  constructor(private injector: Injector) {

  }

  ngDoBootstrap(app: ApplicationRef): void {
    this.registerCustomElements();
    // app.bootstrap(AppComponent);
  }

  private registerCustomElements(): void {
    const customWidget: any = createCustomElement(WidgetComponent, { injector: this.injector });
    customElements.define('app-micro-apps-widget-ce', customWidget);
  }

}
