import { Actions } from '@ngrx/effects';

export interface IAppRegistration {
  id: string;
  name: string;
  tag: string;
  url: string;
  icon?: string;
}

export interface IAppAction {
  type: string;
  payload?: any;
}

export interface IAppActionOriginator {
  id: string;
}

export interface IMicroAppAction extends IAppAction {
  originator: IAppActionOriginator;
}

export interface IMicroAppCustomElement extends HTMLElement {
  microAppsAPI: any;
  appRegistration: IAppRegistration;
}

export interface IMicroAppsAPI {
  actions$: Actions;
  dispatchAction: (action: IAppAction) => void;
  onMount: () => void;
  onUnmount: () => void;
}
