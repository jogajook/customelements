
import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as actions from './micro-apps.actions';
import * as microAppActions from './micro-app/micro-app.actions';
import { MicroAppApiService } from '../services/micro-apps-api.service';


@Injectable()
export class MicroAppsEffects {

  // For debug only
  // allActions$ = createEffect(() => this.actions$.pipe(
  //   map(action => console.log('Action', action))
  // ),
  //   { dispatch: false }
  // );

  registrationList$ = createEffect(() => this.actions$.pipe(
    ofType(actions.EMicroAppsActionType.RegistraionListLoad),
    switchMap(_ => this.microAppApiService.getRegistrationList().pipe(
      map(result => actions.registraionListLoadSuccessAction({ result })),
      catchError(error => of(actions.registraionListLoadErrorAction({ error })))
    ))
  )
  );

  mountSuccessAction$ = createEffect(() => this.actions$.pipe(
    ofType(microAppActions.mountSuccessAction),
    map(action => console.log('MicroAppAction', action))
  ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private microAppApiService: MicroAppApiService
  ) {
  }
}
