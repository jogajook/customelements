import { Action, createReducer, on } from '@ngrx/store';
import * as actions from './micro-apps.actions';
import { IAppRegistration } from '../models/widget.types';

export const microAppsKey = 'microApps';

export interface IApplicationState {
  [microAppsKey]: IMicroAppsState;
}

export interface IMicroAppsState {
  registrations: IAppRegistration[];
  selectedApp: string;
}

export const initiaMicroAppsState: IMicroAppsState = {
  registrations: [],
  selectedApp: null
};

const microAppsReducer = createReducer(
  initiaMicroAppsState,

  on(actions.registraionListLoadSuccessAction, (state: IMicroAppsState, { result }) => ({
    ...state,
    registrations: result
  })),

  on(actions.selectAppAction, (state: IMicroAppsState, { app }) => ({
    ...state,
    selectedApp: app
  }))

);

export function reducer(state: IMicroAppsState | undefined, action: Action): IMicroAppsState {
  return microAppsReducer(state, action);
}
