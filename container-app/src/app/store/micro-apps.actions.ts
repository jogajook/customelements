import { createAction, props } from '@ngrx/store';
import { IAppRegistration } from '../models/widget.types';

export enum EMicroAppsActionType {
  RegistraionListLoad = '[MicroApps] Registraion List Load Action',
  RegistraionListLoadSuccess = '[MicroApps] Registraion List Load Success Action',
  RegistraionListLoadError = '[MicroApps] Registraion List Load Error Action',
  SelectApp = '[MicroApps] Select App Action',
}


export const registraionListLoadAction = createAction(EMicroAppsActionType.RegistraionListLoad);

export const registraionListLoadSuccessAction = createAction(
  EMicroAppsActionType.RegistraionListLoadSuccess,
  props<{ result: IAppRegistration[] }>()
);

export const registraionListLoadErrorAction = createAction(
  EMicroAppsActionType.RegistraionListLoadError,
  props<{ error: any }>()
);

export const selectAppAction = createAction(
  EMicroAppsActionType.SelectApp,
  props<{ app: string }>()
);
