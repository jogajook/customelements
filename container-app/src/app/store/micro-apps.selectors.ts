import { createSelector } from '@ngrx/store';
import { IApplicationState } from './micro-apps-reducer';

export const getMicroAppsState = (state: IApplicationState) => state.microApps;

export const getRegistraionList = createSelector(getMicroAppsState, state => state.registrations);
export const getSelectedAppId = createSelector(getMicroAppsState, state => state.selectedApp);
export const getSelectedApp = createSelector(
  getRegistraionList,
  getSelectedAppId,
  (registrations, selectedAppId) => registrations.find(item => item.id === selectedAppId));
