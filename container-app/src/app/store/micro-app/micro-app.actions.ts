import { createAction, props } from '@ngrx/store';
import { IMicroAppAction } from '../../models/widget.types';

export enum EMicroAppActionType {
  MicroAppMountedSuccess = '[MicroApp] Mount Success Action',
}

type AppOriginator = Pick<IMicroAppAction, 'originator'>;

export const mountSuccessAction = createAction<EMicroAppActionType.MicroAppMountedSuccess, AppOriginator>(
  EMicroAppActionType.MicroAppMountedSuccess,
  props<AppOriginator>()
);
