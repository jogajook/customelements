import { createReducer } from '@ngrx/store';

const microAppReducer = createReducer(
  initiaMicroAppsState,

  on(actions.registraionListLoadSuccessAction, (state: IMicroAppsState, { result }) => ({
    ...state,
    registrations: result
  })),

  on(actions.selectAppAction, (state: IMicroAppsState, { app }) => ({
    ...state,
    selectedApp: app
  }))

);

export function reducer(state: IMicroAppsState | undefined, action: Action): IMicroAppsState {
  return microAppsReducer(state, action);
}
