import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { select, Store, Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IAppRegistration, IMicroAppAction } from '../models/widget.types';
import { IApplicationState } from '../store/micro-apps-reducer';
import * as fromRoot from '../store/micro-apps.selectors';
import * as actions from '../store/micro-apps.actions';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MicroAppsStoreFacadeService {

  constructor(
    private store: Store<IApplicationState>,
    private actions$: Actions
    ) {}

  // selectors

  getRegistraionList(): Observable<IAppRegistration[]> {
    return this.store.pipe(select(fromRoot.getRegistraionList));
  }

  getSelectedApp(): Observable<IAppRegistration> {
    return this.store.pipe(select(fromRoot.getSelectedApp));
  }


  // dispatchers

  registraionListLoad(): void {
    this.store.dispatch(actions.registraionListLoadAction());
  }

  selectApp(app: string): void {
    this.store.dispatch(actions.selectAppAction({ app }));
  }

  dispatchMicroAppAction(action: IMicroAppAction): void {
    this.store.dispatch(action);
  }

  // Other

  isMicroAppAction(action: IMicroAppAction | Action): boolean {
    return typeof (action as IMicroAppAction).originator !== 'undefined';
  }

  getAppActions(appId: string): Actions {
    return this.actions$.pipe(
      filter(action => this.isMicroAppAction(action)),
      filter(action => (action as IMicroAppAction).originator.id !== appId)
    );
  }

}


