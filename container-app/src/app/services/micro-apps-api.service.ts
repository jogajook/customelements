import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IAppRegistration } from '../models/widget.types';

@Injectable({
  providedIn: 'root'
})
export class MicroAppApiService {

  constructor(private http: HttpClient) { }

  getRegistrationList(): Observable<IAppRegistration[]> {
    return this.http.get('/assets/api/micro-apps/registration-list.json').pipe(
      map((data: any) => data.registrations as IAppRegistration[])
    );
  }
}
