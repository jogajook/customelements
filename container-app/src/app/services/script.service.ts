import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

interface IScriptLoadInfo {
    loaded: boolean;
    src: string;
}

@Injectable({
    providedIn: 'root'
})
export class ScriptService {

    private scriptMap: { [url: string]: IScriptLoadInfo } = {};

    constructor(@Inject(DOCUMENT) private document: Document) { }

    loadScript(url: string, async: boolean = true): Promise<boolean> {
        return new Promise((resolve, reject) => {

            if (!this.scriptMap[url]) {
                this.scriptMap[url] = {
                    loaded: false,
                    src: url
                };
            }

            if (this.scriptMap[url].loaded) {
                resolve(true);
                return;
            }

            const script = this.document.createElement('script') as any; // IE Support
            script.type = 'text/javascript';
            script.charset = 'utf-8';
            script.src = this.scriptMap[url].src;
            if (async) {
                script.async = true;
            }

            if (script.readyState) {  // IE
                script.onreadystatechange = () => {
                    if (script.readyState === 'loaded' || script.readyState === 'complete') {
                        script.onreadystatechange = null;
                        this.scriptMap[url].loaded = true;
                        resolve(true);
                    }
                };
            } else {  // Others
                script.onload = () => {
                    this.scriptMap[url].loaded = true;
                    resolve(true);
                };
            }
            script.onerror = error => reject(error);
            this.document.getElementsByTagName('head')[0].appendChild(script);
        });
    }

}
