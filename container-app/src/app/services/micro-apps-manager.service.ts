import { Injectable } from '@angular/core';
import { IMicroAppsAPI, IAppRegistration, IMicroAppAction, IAppAction } from '../models/widget.types';
import { MicroAppsStoreFacadeService } from './micro-apps-store-facade.service';

@Injectable({
  providedIn: 'root'
})
export class MicroAppsManagerService {

  constructor(private microAppsStoreFacadeService: MicroAppsStoreFacadeService) {}

  createMicroAppsAPI(app: IAppRegistration): IMicroAppsAPI {
    return {
      actions$: this.microAppsStoreFacadeService.getAppActions(app.id),
      dispatchAction: (action: IAppAction) => {
        const appAction: IMicroAppAction = { ...action, originator: { id: app.id } };
        this.microAppsStoreFacadeService.dispatchMicroAppAction(appAction);
      },
      onMount: () => this.onAppMount(app),
      onUnmount: () => this.onAppUnmount(app)
    };
  }

  onAppMount(app: IAppRegistration): void {
    console.log('onAppMount', app);
  }

  onAppUnmount(app: IAppRegistration): void {
    console.log('onAppUnmount', app);
  }

}
