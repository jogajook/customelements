import * as React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledDiv = styled.div`
  .rating {
    color: red;
  }
`;

class RatingComponent extends React.Component {
  
  static propTypes = {
    microAppsAPI: PropTypes.object,
    appRegistration: PropTypes.object,
  };

  maxvalue = 5;
  actionUnsubscribe = {
    unsubscribe: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }

  componentDidMount() {
    const microAppsAPI = this.props.microAppsAPI || {};
    
    if (typeof microAppsAPI.onMount === 'function') {
      this.props.microAppsAPI.onMount();
    }

    if (!microAppsAPI.actions$) {
      return;
    }

    this.actionUnsubscribe = microAppsAPI.actions$.subscribe(
      (action) => {
        console.log('React Comp received action', action);
      }
    );
  }

  componentWillUnmount() {
    this.actionUnsubscribe.unsubscribe();

    const microAppsAPI = this.props.microAppsAPI || {};
    if (typeof microAppsAPI.onUnmount === 'function') {
      microAppsAPI.onUnmount();
    }
  }

  setValue(newVal) {
    this.setState({ value: newVal });
    this.dispatchAction({
      type: '[MicroApp] Mount Success Action',
      payload: { value: newVal },
    });
  }

  render() {
    const stars = [...Array(this.maxvalue).keys()].map((i) => (
      <span key={i} className="rating" onClick={() => this.setValue(i)}>
        {i <= this.state.value ? '★' : '☆'}
      </span>
    ));

    return <StyledDiv>{stars}</StyledDiv>;
  }

  dispatchAction(action) {
    const microAppsAPI = this.props.microAppsAPI || {};
    if (
      typeof microAppsAPI.dispatchAction === 'function'
    ) {
      microAppsAPI.dispatchAction(action);
    }
  }
}

export default RatingComponent;
