import * as React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
// import retargetEvents from 'react-shadow-dom-retarget-events';
import RatingComponent from './RatingComponent';

class ReactElement extends HTMLElement {
  get microAppsAPI() {
    return this._microAppsAPI;
  }

  set microAppsAPI(value) {
    this._microAppsAPI = value;
    this.renderReact();
  }

  get appRegistration() {
    return this._registration;
  }

  set appRegistration(value) {
    this._registration = value;
    this.renderReact();
  }

  _microAppsAPI = null;
  _registration = null;

  mountPoint = null;
  useShadow = false;

  constructor() {
    super();
    this.observer = new MutationObserver(() => this.renderReact());
    this.observer.observe(this, { attributes: true });
  }

  connectedCallback() {
    this._innerHTML = this.innerHTML;
    this.mount();
  }

  disconnectedCallback() {
    this.unmount();
    this.observer.disconnect();
  }

  mount() {
    this.initMountPoint();
    this.renderReact();
  }

  unmount() {
    unmountComponentAtNode(this.mountPoint);
  }

  initMountPoint() {
    if (!this.mountPoint) {
      this.mountPoint = this.useShadow
        ? this.attachShadow({ mode: 'open' })
        : document.createElement('div');
      this.appendChild(this.mountPoint);
    }
  }

  renderReact() {
    this.initMountPoint();
    const props = {
      ...this.getConvertedAttributes(this.attributes),
      ...this.getProps(),
      ...this.getEvents(),
      children: this.parseHtmlToReact(this.innerHTML),
    };
    render(<RatingComponent {...props} />, this.mountPoint);
  }

  parseHtmlToReact(html) {
    return html;
  }

  getConvertedAttributes(attributes) {
    return [...attributes]
      .filter((attr) => attr.name !== 'style')
      .map((attr) => this.convert(attr.name, attr.value))
      .reduce((props, prop) => ({ ...props, [prop.name]: prop.value }), {});
  }

  getProps() {
    return {
      microAppsAPI: this.microAppsAPI,
      appRegistration: this.appRegistration,
    };
  }

  getEvents() {
    return Object.values(this.attributes)
      .filter((key) => /on([a-z].*)/.exec(key.name))
      .reduce(
        (events, ev) => ({
          ...events,
          [ev.name]: (args) =>
            this.dispatchEvent(new CustomEvent(ev.name, { ...args })),
        }),
        {}
      );
  }

  convert(attrName, attrValue) {
    let value = attrValue;

    if (attrValue === 'true' || attrValue === 'false') {
      value = attrValue === 'true';
    } else if (!isNaN(attrValue) && attrValue !== '') {
      value = +attrValue;
    } else if (/^{.*}/.exec(attrValue)) {
      value = JSON.parse(attrValue);
    }

    return {
      name: attrName,
      value: value,
    };
  }
}

window.customElements.define('app-rating-ce', ReactElement);
