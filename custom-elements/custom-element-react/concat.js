var concat = require('concat-files');
var path = require('path');
const glob = require("glob");

const staticDir = 'build/static';
const jsFilesRegEx = staticDir + '/**/*.js';
const bundleFile = staticDir + '/js/bundle.js';

const getOrderedFiles = (files) => {
  let result = [];
  
  const chunks = files.filter(item => item.indexOf('main') === -1);
  result = [...result, ...chunks];
  
  const mainChunks = files.filter(item => item.indexOf('main') >= 0 && item.indexOf('runtime') === -1);
  result = [...result, ...mainChunks];

  const runtimeChunks = files.filter(item => item.indexOf('runtime-main') >= 0);
  result = [...result, ...runtimeChunks];
  
  return result.map(f => path.resolve(__dirname, f));
};


// concat(files, './build/static/js/bundle.js', function(err) {
concat(getOrderedFiles(glob.sync(jsFilesRegEx)), bundleFile, function(err) {
    if (err) throw err;
    console.log('React Built');
});
