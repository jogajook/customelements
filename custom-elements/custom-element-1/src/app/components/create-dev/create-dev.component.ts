import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-create-dev',
  templateUrl: './create-dev.component.html',
  styleUrls: ['./create-dev.component.scss']
})
export class CreateDevComponent implements OnInit {

  @Input() microAppsAPI = null;
  @Input() appRegistration = null;

  counter = 0;

  constructor() {}

  ngOnInit() {
    console.log('microAppsAPI', this.microAppsAPI);
    console.log('appRegistration', this.appRegistration);
    this.microAppsAPI.dispatchAction({
      type: '[MicroApp] Mount Success Action',
      payload: {
        app: 'create dev'
      }
    });
  }

  increaseCounter(): void {
    this.counter++;
    this.microAppsAPI.dispatchAction({
      type: '[MicroApp] Custom. Should Be Ignored Action',
      payload: { counter: this.counter }
    });
  }

}
