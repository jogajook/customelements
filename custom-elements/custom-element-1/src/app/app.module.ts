import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule, ApplicationRef } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateDevComponent } from './components/create-dev/create-dev.component';

@NgModule({
  declarations: [
    CreateDevComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [CreateDevComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(app: ApplicationRef): void {
    const el: any = createCustomElement(CreateDevComponent, { injector: this.injector });
    customElements.define('app-create-dev-ce', el);
    // app.bootstrap(CreateDevComponent);
  }

}
