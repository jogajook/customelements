var concat = require('concat-files');

var filesEs5 = [
    './dist/ng-widget/runtime.js',
    // './dist/ng-widget/polyfills.js',// Commnet in in case of the custom elements being bootstraped twice
    // './dist/ng-widget/polyfills-es5.js',
    './dist/ng-widget/scripts.js',
    './dist/ng-widget/main.js',
];
var filesEs2015 = [
    './dist/ng-widget/runtime-es2015.js',
    './dist/ng-widget/polyfills-es2015.js',
    // './dist/ng-widget/main-es2015.js',
    './dist/ng-widget/scripts.js',
    './dist/ng-widget/main-es2015.js',
];

concat(filesEs5, './dist/ng-widget/assets/app.js', function(err) {
    if (err) throw err;
    console.log('Angular Built');
});
