import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dev-list',
  templateUrl: './dev-list.component.html',
  styleUrls: ['./dev-list.component.scss']
})
export class DevListComponent implements OnInit {
  @Input() microAppsAPI = null;
  @Input() appRegistration = null;

  counter = 0;
  devs: any[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    console.log('microAppsAPI', this.microAppsAPI);
    console.log('appRegistration', this.appRegistration);
    this.http.get(this.getUrl()).subscribe((data: any) => {
      this.devs = data.devs;
    });
  }

  private getUrl(): string {
    return '/assets/api/devs/list.json';
  }

}
