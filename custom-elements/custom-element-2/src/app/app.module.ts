import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule, ApplicationRef } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DevListComponent } from './components/dev-list/dev-list.component';

@NgModule({
  declarations: [
    DevListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [DevListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(app: ApplicationRef): void {
    const el: any = createCustomElement(DevListComponent, { injector: this.injector });
    customElements.define('app-dev-list-ce', el);
    // app.bootstrap(DevListComponent);
  }

}
